# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Primitivas OpenGl
# Descripción: Visualización de figuras primitivas 3D

# Importar librerías gráficas adicionales
import OpenGL.GL as gl
import OpenGL.GLU as glu
# Importar librerías de primitivas personalizadas
from cubo import *
from piramide import *
from cilindro import *
from octaedro import *
from Dodecaedro import *
from cono import *
from Esfera import *
from prisma import *
from Icosaedro import *
# Importar librería pygame
import pygame
from pygame.locals import *
# Importar librería de luces
import LUCES
# Importar otra librerías
import sys, os, traceback

# Inicializando de escena con librería Pygame
if sys.platform in ["win32","win64"]: os.environ["SDL_VIDEO_CENTERED"]="1"
from math import *

# Inicialización
pygame.display.init()
pygame.font.init()
screen_size = [800,600]
multisample = 0
icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
pygame.display.set_caption("Figuras primitivas")
if multisample:
    pygame.display.gl_set_attribute(GL_MULTISAMPLEBUFFERS,1)
    pygame.display.gl_set_attribute(GL_MULTISAMPLESAMPLES,multisample)

pygame.display.set_mode(screen_size,OPENGL|DOUBLEBUF)
gl.glHint(gl.GL_PERSPECTIVE_CORRECTION_HINT,gl.GL_NICEST)
gl.glEnable(gl.GL_DEPTH_TEST)

camera_rot = [30.0,20.0]      #The spherical coordinates' angles (degrees).
camera_radius = 3.0           #The sphere's radius
camera_center = [0.0,0.0,0.0]

# Llamada a las clases
# Creación de objetos

cu = Cubo(gl)
pi = Piramide(gl)
ci = Cilindro(gl)
octaedro = Octaedro(gl)
do = Dodecaedro(gl)
co = Cono(gl)
es= Esfera(gl)
pr= Prisma(gl)
icos = Icosaedro(gl)
LUCES.IniciarIluminacion()

def get_input():
    global camera_rot, camera_radius

    keys_pressed = pygame.key.get_pressed()
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_position = pygame.mouse.get_pos()

    mouse_rel = pygame.mouse.get_rel()

    for event in pygame.event.get():
        if   event.type == QUIT: return False
        elif event.type == KEYDOWN:
            if   event.key == K_ESCAPE: return False
        elif event.type == MOUSEBUTTONDOWN:
            if   event.button == 4: camera_radius *= 0.9
            elif event.button == 5: camera_radius /= 0.9

    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    return True

# Variable para opciones de teclado
global Tecla
Tecla=10

def draw():
    global Tecla

    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
    gl.glViewport(0, 0, screen_size[0], screen_size[1])
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()
    glu.gluPerspective(45, float(screen_size[0]) / float(screen_size[1]), 0.1, 100.0)
    gl.glMatrixMode(gl.GL_MODELVIEW)
    gl.glLoadIdentity()

    camera_pos = [
        camera_center[0] + camera_radius * cos(radians(camera_rot[0])) * cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius * sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius * sin(radians(camera_rot[0])) * cos(radians(camera_rot[1]))
    ]
    glu.gluLookAt(
        camera_pos[0], camera_pos[1], camera_pos[2],
        camera_center[0], camera_center[1], camera_center[2],
        0, 1, 0
    )

# Llamada a clases con parámetros
    Teclado()

    if (Tecla == 1):
        cu.crearCubo(1, 0.5, 0.8,1,1,1,0,0,0,0,0,0)
        cu.crearCuboWireFrame(1, 1, 0, 1,1,1,1,0,0,0,0,0,0)
    if (Tecla == 2):
        pi.crearPiramide(1, 0, 0, 1,1,1,1,0,0,0,0,0,0)
        pi.crearPiramideWireFrame(1, 1, 0, 1, 1,1,1,1,0,0,0,0,0,0)

    if (Tecla == 3):
        ci.crearCilindro(1,0,0,0.4,0.4,0.8,30,10,1,1,1,0,0,0,0,0,0)#los tres primeros parametros para cambiar el color
        ci.crearCilindroWireFrame(1,1,0,1,0.4,0.4,0.8,30,1,1,1,1,0,0,0,0,0,0)#los tres primeros parametros cambia el color dar valores entre 0 y 1

    if (Tecla == 4):
        octaedro.crearOctaedro(1,0,0,1,4,2,1,1,1,0,0,0,0,0,0)#los tres primeros parametros para cambiar el color
        octaedro.crearOctaedroWireFrame(1,1,0,1,1,4,2,1,1,1,0,0,0,0,0,0)#los tres primeros parametros cambia el color
    if (Tecla == 5):
        do.crearDodecaedro(1,1,1,1,1,1,0,0,0,0,0,0)
        do.crearDodecaedroWireFrame(1,1,1,1,1,1,1,0,0,0,0,0,0)
    if (Tecla == 6):
        co.crearCono(0.5,1,0.5, 0, 0, 1,1,1,1,0,0,0,0,0)
    if (Tecla == 7):
        es.crearEsfera(2, 0.5, 1, 0, 0, 0, 0, 1, 0, 0.5, 30, 30)
        es.crearEsferaWireframe(2, 0.5, 1, 0, 0, 0, 1, 1, 0.5, 2, 0.5, 30, 30)
    if (Tecla ==8):
        pr.crearPrisma(-1.0,0,0,0.75,0.75,0.75,0,60,0,0,0.7,0.4)

    if (Tecla == 9):
        icos.crearIcosaedro(-1.0,0,0,0.75,0.75,0.75,0,60,0,0.32,0.56,0.9)

    pygame.display.flip()
# Función del teclado
def Teclado():
    global  Tecla
    teclado = pygame.key.get_pressed()
    if teclado[K_1]:
        pygame.display.set_caption('Cubo')
        Tecla = 1

    if teclado[K_2]:
        pygame.display.set_caption('Pirámide')
        Tecla = 2

    if teclado[K_3]:
        pygame.display.set_caption('Cilindro')
        Tecla = 3

    if teclado[K_4]:
        pygame.display.set_caption('Octaedro')
        Tecla = 4

    if teclado[K_5]:
        pygame.display.set_caption('Dodecaedro')
        Tecla = 5
    if teclado[K_6]:
        pygame.display.set_caption('Cono')
        Tecla = 6
    if teclado[K_7]:
        pygame.display.set_caption('Esfera')
        Tecla = 7
    if teclado[K_8]:
        pygame.display.set_caption('Prisma')
        Tecla=8

    if teclado[K_9]:
        pygame.display.set_caption('Icosaedro')
        Tecla=9

# Función main
def main():
    clock = pygame.time.Clock()
    while True:
        if not get_input(): break
        # Dibujar Escena
        draw()
        clock.tick(60)
    pygame.quit()


if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()


